let collection = [];

// Write the queue functions below.

function print() {
	// output all the elements of the queue
	console.log(collection)

	return collection;
}

function enqueue(element) {
	// add an element to the rear of the queue 
	collection.push(element)

	return collection;
}

function dequeue() {
	// remove an element at the front of the queue
	collection.shift()

	return collection

}

function front() {
	// show the element at the front of the queue
	console.log(collection[0])

	return collection[0]

}

function size() {
	// show the total number of elements  
	console.log(collection.length)
	
	return collection.length;
}

function isEmpty() {
	// return a Boolean value describing whether the queue is empty or not
	if(collection.length < 0) {

		return true;

	} else {

		return false;
	}
}

module.exports = {
	collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};